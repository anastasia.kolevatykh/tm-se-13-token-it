package ru.kolevatykh.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.*;
import java.util.Collection;

public interface ServiceLocator {

    @Nullable String getToken();

    void setToken(@Nullable String token);

    @NotNull IUserEndpoint getUserEndpoint();

    @NotNull IProjectEndpoint getProjectEndpoint();

    @NotNull ITaskEndpoint getTaskEndpoint();

    @NotNull IDomainEndpoint getDomainEndpoint();

    @NotNull ITokenEndpoint getTokenEndpoint();

    @NotNull Collection<AbstractCommand> getCommands();
}
