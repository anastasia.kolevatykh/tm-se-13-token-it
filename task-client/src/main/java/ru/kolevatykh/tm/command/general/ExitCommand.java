package ru.kolevatykh.tm.command.general;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Exception_Exception;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "e";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\t\tExit.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }@Override
    public void execute() throws Exception {
        serviceLocator.getTokenEndpoint().closeTokenSession(serviceLocator.getToken());
        System.out.println("[Goodbye!]");
        System.exit(0);
    }
}
