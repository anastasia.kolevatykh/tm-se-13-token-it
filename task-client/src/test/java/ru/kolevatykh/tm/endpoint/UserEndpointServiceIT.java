package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ru.kolevatykh.tm.util.PasswordHashUtil;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import org.junit.experimental.categories.Category;
import static org.junit.jupiter.api.Assertions.*;

@Category(IntegrationTest.class)
@Tag("integration-test")
class UserEndpointServiceIT {

    @Nullable
    private ITokenEndpoint tokenEndpoint;

    @Nullable
    private IDomainEndpoint domainEndpoint;

    @Nullable
    private IUserEndpoint userEndpoint;

    @Nullable
    private String token;

    @BeforeEach
    void setUp() throws Exception_Exception, MalformedURLException {
        tokenEndpoint = new TokenEndpointService(new URL("http://0.0.0.0:1234/TokenEndpoint?wsdl")).getTokenEndpointPort();
        userEndpoint = new UserEndpointService(new URL("http://0.0.0.0:1234/UserEndpoint?wsdl")).getUserEndpointPort();
        domainEndpoint = new DomainEndpointService(new URL("http://0.0.0.0:1234/DomainEndpoint?wsdl")).getDomainEndpointPort();

        @NotNull final String login = "testUser";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testUser");

        assertNotNull(tokenEndpoint);
        token = tokenEndpoint.openTokenSessionReg(login, pass);
        assertNotNull(token);
        System.out.println("USER endpoint token: " + token);
    }

    @AfterEach
    void tearDown() throws Exception_Exception {
        assertNotNull(userEndpoint);
        System.out.println("USER: " + userEndpoint.findUserById(token));
        userEndpoint.removeUser(token);
        token = null;
    }

    @Test
    void tryUserAuthTest() throws Exception_Exception {
        assertNotNull(tokenEndpoint);
        @NotNull final String login = "testUser1";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testUser1");
        assertNotNull(userEndpoint);
        @NotNull final User currentUser = userEndpoint.findUserById(token);
        if (currentUser != null)
            System.out.println("[You are authorized under login: "
                    + currentUser.getLogin()
                    + ". Please LOGOUT first, in order to authorize under OTHER account.]");
        assertNotNull(token);
    }

    @Test
    void findAllUsersTest() throws Exception_Exception {
        assertNotNull(tokenEndpoint);
        @NotNull final User user = tokenEndpoint.getUserByToken(token);
        assertNotNull(userEndpoint);
        userEndpoint.mergeUser(token, user.getLogin(), "testUser",
                PasswordHashUtil.getPasswordHash("testUser"), "ADMIN");

        @NotNull final String login = "testUser";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testUser");
        token = tokenEndpoint.openTokenSessionAuth(login, pass);

        @NotNull final List<User> userList = userEndpoint.findAllUsers(token);
        assertFalse(userList.isEmpty());
    }

    @Test
    void getUserByIdTest() throws Exception_Exception {
        assertNotNull(userEndpoint);
        @NotNull final User user = userEndpoint.findUserById(token);
        assertNotNull(user);
    }

    @Test
    void editUserTest() throws Exception_Exception {
        assertNotNull(tokenEndpoint);
        @NotNull final User user = tokenEndpoint.getUserByToken(token);
        assertNotNull(userEndpoint);
        userEndpoint.mergeUser(token, user.getLogin(), "testUser",
                PasswordHashUtil.getPasswordHash("testUser"), "ADMIN");

        @NotNull final String login = "testUser";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testUser");

        assertNotNull(tokenEndpoint);
        @NotNull final String tokenNew = tokenEndpoint.openTokenSessionAuth(login, pass);
        assertNotEquals(tokenNew, token);
        token = tokenNew;
    }

    @Test
    void removeUserTest() throws Exception_Exception {
        assertNotNull(userEndpoint);
        userEndpoint.removeUser(token);
        token = null;

        @NotNull final String login = "admin";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("pass1");

        assertNotNull(tokenEndpoint);
        token = tokenEndpoint.openTokenSessionAuth(login, pass);

        @NotNull final List<User> users = userEndpoint.findAllUsers(token);
        for (@NotNull final User user : users) {
            assertNotEquals("testUser", user.getLogin());
        }
    }

    @Test
    void removeAllUsers() throws Exception_Exception {
        assertNotNull(userEndpoint);
        userEndpoint.removeUser(token);

        assertNotNull(tokenEndpoint);
        @NotNull final String login = "test1";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("test1");
        token = tokenEndpoint.openTokenSessionReg(login, pass);

        @NotNull User currentUser = tokenEndpoint.getUserByToken(token);
        userEndpoint.mergeUser(token, currentUser.getLogin(), "test1",
                PasswordHashUtil.getPasswordHash("test1"), "ADMIN");

        token = tokenEndpoint.openTokenSessionAuth("test1", PasswordHashUtil.getPasswordHash("test1"));
        userEndpoint.removeAllUsers(token);
        token = null;

        token = tokenEndpoint.openTokenSessionReg(login, pass);
        currentUser = tokenEndpoint.getUserByToken(token);
        userEndpoint.mergeUser(token, currentUser.getLogin(), "test1",
                PasswordHashUtil.getPasswordHash("test1"), "ADMIN");

        token = tokenEndpoint.openTokenSessionAuth("test1", PasswordHashUtil.getPasswordHash("test1"));

        @NotNull final List<User> users = userEndpoint.findAllUsers(token);
        for (@NotNull final User user : users) {
            assertEquals("test1", user.getLogin());
        }

        assertNotNull(domainEndpoint);
        domainEndpoint.loadJacksonJson(token);
    }
}