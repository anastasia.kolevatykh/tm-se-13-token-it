package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ru.kolevatykh.tm.util.PasswordHashUtil;

import javax.xml.datatype.XMLGregorianCalendar;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Category(IntegrationTest.class)
@Tag("integration-test")
class TaskEndpointServiceIT {

    @Nullable
    private ITokenEndpoint tokenEndpoint;

    @Nullable
    private IUserEndpoint userEndpoint;

    @Nullable
    private IProjectEndpoint projectEndpoint;

    @Nullable
    private ITaskEndpoint taskEndpoint;

    @Nullable
    private String token;

    @BeforeEach
    void setUp() throws MalformedURLException, Exception_Exception {
        tokenEndpoint = new TokenEndpointService(new URL("http://0.0.0.0:1234/TokenEndpoint?wsdl")).getTokenEndpointPort();
        userEndpoint = new UserEndpointService(new URL("http://0.0.0.0:1234/UserEndpoint?wsdl")).getUserEndpointPort();
        projectEndpoint = new ProjectEndpointService(new URL("http://0.0.0.0:1234/ProjectEndpoint?wsdl")).getProjectEndpointPort();
        taskEndpoint = new TaskEndpointService(new URL("http://0.0.0.0:1234/TaskEndpoint?wsdl")).getTaskEndpointPort();

        @NotNull final String login = "testTaskUser";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testTaskUser");

        assertNotNull(tokenEndpoint);
        token = tokenEndpoint.openTokenSessionReg(login, pass);
        assertNotNull(token);

        System.out.println("TASK endpoint token: " + token);

        assertNotNull(projectEndpoint);
        projectEndpoint.persistProject(token, "demo project", "", "1.1.2019", "1.1.2020");

        assertNotNull(taskEndpoint);
        taskEndpoint.persistTask(token, null, "demo task", "", "1.1.2019", "1.1.2020");
    }

    @AfterEach
    void tearDown() throws Exception_Exception {
        assertNotNull(taskEndpoint);
        System.out.println("TASK: " + taskEndpoint.findAllTasksByUserId(token));
        taskEndpoint.removeAllTasks(token);

        assertNotNull(projectEndpoint);
        System.out.println("PROJECT: " + projectEndpoint.findAllProjectsByUserId(token));
        projectEndpoint.removeAllProjects(token);

        assertNotNull(userEndpoint);
        userEndpoint.removeUser(token);
        token = null;
    }

    @Test
    void findTasksWithoutProject() throws Exception_Exception {
        assertNotNull(taskEndpoint);
        @NotNull final List<Task> tasks = taskEndpoint.findTasksWithoutProject(token);
        assertFalse(tasks.isEmpty());
    }

    @Test
    void findTaskById() throws Exception_Exception {
        assertNotNull(taskEndpoint);
        @NotNull final Task task = taskEndpoint.findTaskByName(token, "demo task");
        assertNotNull(taskEndpoint.findTaskById(token, task.getId()));
    }

    @Test
    void findTasksByProjectId() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        @NotNull final Project project = projectEndpoint.findProjectByName(token, "demo project");
        assertNotNull(taskEndpoint);
        @NotNull final Task task = taskEndpoint.findTaskByName(token, "demo task");
        taskEndpoint.mergeTask(token, project.getId(), task.getId(), task.getName(),
                task.getDescription(), "1.1.2019", "1.1.2020");
        @NotNull final List<Task> tasks = taskEndpoint.findTasksByProjectId(token, project.getId());
        assertFalse(tasks.isEmpty());
    }

    @Test
    void persistTask() throws Exception_Exception {
        assertNotNull(taskEndpoint);
        taskEndpoint.persistTask(token, null, "demo task 3", "", "1.1.2019", "1.1.2020");
        @NotNull final List<Task> tasks = taskEndpoint.findAllTasksByUserId(token);
        for (@NotNull final Task task : tasks) {
            if (task.getName().equals("demo task 3")) {
                assertEquals("demo task 3", task.getName());
            }
        }
        assertFalse(tasks.isEmpty());
    }

    @Test
    void mergeTask() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        @NotNull final Project project = projectEndpoint.findProjectByName(token, "demo project");
        assertNotNull(taskEndpoint);
        @NotNull final Task task = taskEndpoint.findTaskByName(token, "demo task");
        taskEndpoint.mergeTask(token, project.getId(), task.getId(), task.getName(),
                task.getDescription(), "1.1.2019", "1.1.2020");
        @NotNull final Task tempTask = taskEndpoint.findTaskById(token, task.getId());
        assertEquals(project.getId(), tempTask.getProjectId());
    }

    @Test
    void removeTasksWithProjectId() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        @NotNull final Project project = projectEndpoint.findProjectByName(token, "demo project");
        assertNotNull(taskEndpoint);
        @NotNull final Task task = taskEndpoint.findTaskByName(token, "demo task");
        taskEndpoint.mergeTask(token, project.getId(), task.getId(), task.getName(),
                task.getDescription(), "1.1.2019", "1.1.2020");
        taskEndpoint.removeTasksWithProjectId(token);
        @NotNull final Task tempTask = taskEndpoint.findTaskByName(token, "demo task");
        assertNull(tempTask);
        @NotNull final List<Task> tasks = taskEndpoint.findTasksByProjectId(token, project.getId());
        assertTrue(tasks.isEmpty());
    }

    @Test
    void findTasksSortedByCteateDate() throws Exception_Exception {
        assertNotNull(taskEndpoint);
        taskEndpoint.persistTask(token, null, "demo task 2", "", "1.1.2019", "1.1.2020");
        @NotNull final List<Task> tasks = taskEndpoint.findTasksSortedByCteateDate(token);
        @NotNull XMLGregorianCalendar prevTime = tasks.get(0).getCreateDate();
        for (int i = 1; i < tasks.size(); i++) {
            @NotNull XMLGregorianCalendar currTime = tasks.get(i).getCreateDate();
            assertFalse(prevTime.compare(currTime) > 0);
            prevTime = currTime;
        }
    }

    @Test
    void findTasksSortedByStartDate() throws Exception_Exception {
        assertNotNull(taskEndpoint);
        taskEndpoint.persistTask(token, null, "demo task 2", "", "1.1.2018", "1.1.2021");
        @NotNull final List<Task> tasks = taskEndpoint.findTasksSortedByStartDate(token);
        @NotNull XMLGregorianCalendar prevTime = tasks.get(0).getStartDate();
        for (int i = 1; i < tasks.size(); i++) {
            @NotNull XMLGregorianCalendar currTime = tasks.get(i).getStartDate();
            assertFalse(prevTime.compare(currTime) > 0);
            prevTime = currTime;
        }
    }

    @Test
    void findTasksSortedByFinishDate() throws Exception_Exception {
        assertNotNull(taskEndpoint);
        taskEndpoint.persistTask(token, null, "demo task 2", "", "1.1.2009", "1.1.2010");
        @NotNull final List<Task> tasks = taskEndpoint.findTasksSortedByFinishDate(token);
        @NotNull XMLGregorianCalendar prevTime = tasks.get(0).getFinishDate();
        for (int i = 1; i < tasks.size(); i++) {
            @NotNull XMLGregorianCalendar currTime = tasks.get(i).getFinishDate();
            assertFalse(prevTime.compare(currTime) > 0);
            prevTime = currTime;
        }
    }

    @Test
    void assignToProject() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        @NotNull final Project project = projectEndpoint.findProjectByName(token, "demo project");
        assertNotNull(taskEndpoint);
        @NotNull final Task task = taskEndpoint.findTaskByName(token, "demo task");
        taskEndpoint.mergeTask(token, project.getId(), task.getId(), "demo task assigned",
                task.getDescription(), "1.1.2010", "1.1.2020");

        @Nullable final Task testTask = taskEndpoint.findTaskById(token, task.getId());
        assertNotNull(testTask);
        assertNotNull(testTask.getProjectId());
    }

    @Test
    void findAllTasksByUserId() throws Exception_Exception {
        assertNotNull(taskEndpoint);
        taskEndpoint.persistTask(token, null, "demo task 2", "", "12.12.2012", "12.12.2020");
        @NotNull final List<Task> tasks = taskEndpoint.findAllTasksByUserId(token);
        assertFalse(tasks.isEmpty());
        assertEquals(2, tasks.size());
    }

    @Test
    void findTaskByName() throws Exception_Exception {
        assertNotNull(taskEndpoint);
        taskEndpoint.persistTask(token, null, "demo task 3", "", "12.12.2012", "12.12.2020");
        assertNotNull(taskEndpoint);
        @NotNull final Task task = taskEndpoint.findTaskByName(token, "demo task 3");
        assertNotNull(task);
    }

    @Test
    void mergeTaskStatus() throws Exception_Exception {
        assertNotNull(taskEndpoint);
        @NotNull final Task task = taskEndpoint.findTaskByName(token, "demo task");
        @NotNull Task tempTask = taskEndpoint.findTaskByName(token, "demo task");
        taskEndpoint.mergeTaskStatus(token, tempTask.getId(), "READY");

        tempTask = taskEndpoint.findTaskById(token, tempTask.getId());
        assertNotEquals(task.getStatusType().value(), tempTask.getStatusType().value());
    }

    @Test
    void removeProjectTasks() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        @NotNull final Project project = projectEndpoint.findProjectByName(token, "demo project");
        assertNotNull(taskEndpoint);
        @NotNull final Task task = taskEndpoint.findTaskByName(token, "demo task");
        taskEndpoint.mergeTask(token, project.getId(), task.getId(), "demo task assigned",
                task.getDescription(), "1.1.2010", "1.1.2020");
        taskEndpoint.removeProjectTasks(token, project.getId());
        @Nullable final Task tempTask = taskEndpoint.findTaskByName(token, "demo task");
        assertNull(tempTask);
    }

    @Test
    void findTasksSortedByStatus() throws Exception_Exception {
        assertNotNull(taskEndpoint);
        taskEndpoint.persistTask(token, null, "demo task 2", "", "1.1.2009", "1.1.2010");
        @NotNull final Task tempTask = taskEndpoint.findTaskByName(token, "demo task 2");
        taskEndpoint.mergeTaskStatus(token, tempTask.getId(), "READY");
        @NotNull final List<Task> tasks = taskEndpoint.findTasksSortedByStatus(token);
        @NotNull int prevStatus = 0;
        for (@NotNull final Task task : tasks) {
            @NotNull int currStatus = task.getStatusType().ordinal();
            assertFalse(prevStatus > currStatus);
            prevStatus = currStatus;
        }
    }

    @Test
    void findTasksBySearch() throws Exception_Exception {
        assertNotNull(taskEndpoint);
        taskEndpoint.persistTask(token, null, "mega task 2", "", "1.1.2009", "1.1.2010");
        @NotNull final List<Task> tasks = taskEndpoint.findTasksBySearch(token, "mega");
        for (@NotNull final Task task : tasks) {
            assertTrue(task.getName().contains("mega") || task.getDescription().contains("mega"));
        }
    }

    @Test
    void removeTask() throws Exception_Exception {
        assertNotNull(taskEndpoint);
        @NotNull final Task tempTask = taskEndpoint.findTaskByName(token, "demo task");
        taskEndpoint.removeTask(token, tempTask.getId());
        @NotNull final List<Task> tasks = taskEndpoint.findAllTasksByUserId(token);
        assertTrue(tasks.isEmpty());
    }

    @Test
    void removeAllTasks() throws Exception_Exception {
        assertNotNull(taskEndpoint);
        taskEndpoint.persistTask(token, null, "mega task 2", "", "1.1.2009", "1.1.2010");
        taskEndpoint.persistTask(token, null, "mini task 3", "", "1.1.2011", "1.1.2013");
        taskEndpoint.removeAllTasks(token);
        @NotNull final List<Task> tasks = taskEndpoint.findAllTasksByUserId(token);
        assertTrue(tasks.isEmpty());
    }
}