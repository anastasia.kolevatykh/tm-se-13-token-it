package ru.kolevatykh.tm.endpoint;

import com.sun.xml.ws.fault.ServerSOAPFaultException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ru.kolevatykh.tm.util.PasswordHashUtil;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Category(IntegrationTest.class)
@Tag("integration-test")
class TokenEndpointServiceIT {

    @Nullable
    private ITokenEndpoint tokenEndpoint = null;

    @Nullable
    private IUserEndpoint userEndpoint = null;

    @Nullable
    private String token = null;

    @BeforeEach
    void setUp() throws Exception_Exception, MalformedURLException {
        tokenEndpoint = new TokenEndpointService(new URL("http://0.0.0.0:1234/TokenEndpoint?wsdl")).getTokenEndpointPort();
        userEndpoint = new UserEndpointService(new URL("http://0.0.0.0:1234/UserEndpoint?wsdl")).getUserEndpointPort();

        @NotNull final String login = "test";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("test");

        assertNotNull(tokenEndpoint);
        token = tokenEndpoint.openTokenSessionReg(login, pass);
        assertNotNull(token);
    }

    @AfterEach
    void tearDown() throws Exception_Exception {
        assertNotNull(userEndpoint);
        userEndpoint.removeUser(token);
        token = null;
    }

    @Test
    void getUserAuthException() {
        @NotNull final String login = "admin0";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("pass");

        assertNotNull(tokenEndpoint);
        Throwable exception = assertThrows(ServerSOAPFaultException.class, () -> {
            tokenEndpoint.openTokenSessionAuth(login, pass);
        });

        String expectedMessage = "[The user doesn't exist.]";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void getUserByToken() throws Exception_Exception {
        assertNotNull(tokenEndpoint);
        @NotNull final User user = tokenEndpoint.getUserByToken(token);
        assertNotNull(user);
    }

    @Test
    void openTokenSessionAuth() throws Exception_Exception {
        assertNotNull(userEndpoint);
        userEndpoint.removeUser(token);
        @NotNull final String login = "admin";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("pass1");
        assertNotNull(tokenEndpoint);
        token = tokenEndpoint.openTokenSessionAuth(login, pass);
        assertNotNull(token);
    }

    @Test
    void closeAllTokenSession() throws Exception_Exception {
        assertNotNull(tokenEndpoint);
        tokenEndpoint.closeAllTokenSession(token);

        @NotNull final String login = "test";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("test");

        assertNotNull(tokenEndpoint);
        token = tokenEndpoint.openTokenSessionAuth(login, pass);
        @NotNull final User user = tokenEndpoint.getUserByToken(token);

        @NotNull final List<Session> sessions = tokenEndpoint.getListTokenSession(token);
        for (@NotNull final Session session : sessions)
            assertEquals(session.getUserId(), user.getId());
    }

    @Test
    void closeTokenSession() throws Exception_Exception {
        assertNotNull(tokenEndpoint);
        tokenEndpoint.closeTokenSession(token);

        Throwable exception = assertThrows(ServerSOAPFaultException.class, () -> {
            tokenEndpoint.getUserByToken(token);
        });

        String expectedMessage = "[The session does not exist.]";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));

        @NotNull final String login = "test";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("test");

        assertNotNull(tokenEndpoint);
        token = tokenEndpoint.openTokenSessionAuth(login, pass);
    }

    @Test
    void getListTokenSession() throws Exception_Exception {
        assertNotNull(tokenEndpoint);
        @NotNull final User user = tokenEndpoint.getUserByToken(token);
        @NotNull final List<Session> sessions = tokenEndpoint.getListTokenSession(token);
        for (@NotNull final Session session : sessions)
            if (session.getUserId().equals(user.getId())) {
                assertEquals(session.getUserId(), user.getId());
            }
        assertFalse(sessions.isEmpty());
    }

    @Test
    void openTokenSessionReg() throws Exception_Exception {
        assertNotNull(userEndpoint);
        userEndpoint.removeUser(token);
        token = null;

        @NotNull final String login = "testTokenUser";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testTokenUser");

        assertNotNull(tokenEndpoint);
        token = tokenEndpoint.openTokenSessionReg(login, pass);
        assertNotNull(token);
    }
}