package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ru.kolevatykh.tm.util.PasswordHashUtil;

import javax.xml.datatype.XMLGregorianCalendar;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Category(IntegrationTest.class)
@Tag("integration-test")
class ProjectEndpointServiceIT {

    @Nullable
    private ITokenEndpoint tokenEndpoint;

    @Nullable
    private IUserEndpoint userEndpoint;

    @Nullable
    private IProjectEndpoint projectEndpoint;

    @Nullable
    private ITaskEndpoint taskEndpoint;

    @Nullable
    private String token;

    @BeforeEach
    void setUp() throws Exception_Exception, MalformedURLException {
        tokenEndpoint = new TokenEndpointService(new URL("http://0.0.0.0:1234/TokenEndpoint?wsdl")).getTokenEndpointPort();
        userEndpoint = new UserEndpointService(new URL("http://0.0.0.0:1234/UserEndpoint?wsdl")).getUserEndpointPort();
        projectEndpoint = new ProjectEndpointService(new URL("http://0.0.0.0:1234/ProjectEndpoint?wsdl")).getProjectEndpointPort();
        taskEndpoint = new TaskEndpointService(new URL("http://0.0.0.0:1234/TaskEndpoint?wsdl")).getTaskEndpointPort();

        @NotNull final String login = "testTaskUser";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testTaskUser");

        assertNotNull(tokenEndpoint);
        token = tokenEndpoint.openTokenSessionReg(login, pass);
        assertNotNull(token);

        System.out.println("PROJECT endpoint token: " + token);

        assertNotNull(projectEndpoint);
        projectEndpoint.persistProject(token, "demo project", "", "1.1.2019", "1.1.2020");
    }

    @AfterEach
    void tearDown() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        System.out.println("PROJECT: " + projectEndpoint.findAllProjectsByUserId(token));
        projectEndpoint.removeAllProjects(token);

        assertNotNull(userEndpoint);
        userEndpoint.removeUser(token);
        token = null;
    }

    @Test
    void findProjectsSortedByStatus() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        projectEndpoint.persistProject(token,"demo project 2", "", "1.1.2009", "1.1.2010");
        @NotNull final Project tempProject = projectEndpoint.findProjectByName(token, "demo project 2");
        projectEndpoint.mergeProjectStatus(token, tempProject.getId(), "READY");
        @NotNull final List<Project> projects = projectEndpoint.findProjectsSortedByStatus(token);
        @NotNull int prevStatus = 0;
        for (@NotNull final Project project : projects) {
            @NotNull int currStatus = project.getStatusType().ordinal();
            assertFalse(prevStatus > currStatus);
            prevStatus = currStatus;
        }
    }

    @Test
    void mergeProjectStatus() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        @NotNull final Project project = projectEndpoint.findProjectByName(token, "demo project");
        @NotNull Project tempProject = projectEndpoint.findProjectByName(token, "demo project");
        projectEndpoint.mergeProjectStatus(token, tempProject.getId(), "READY");

        tempProject = projectEndpoint.findProjectById(token, tempProject.getId());
        assertNotEquals(project.getStatusType().value(), tempProject.getStatusType().value());
    }

    @Test
    void findAllProjectsByUserId() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        projectEndpoint.persistProject(token, "mega project 2", "", "12.12.2012", "12.12.2020");
        @NotNull final List<Project> projects = projectEndpoint.findAllProjectsByUserId(token);
        assertFalse(projects.isEmpty());
        assertEquals(2, projects.size());
    }

    @Test
    void findProjectsSortedByFinishDate() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        projectEndpoint.persistProject(token, "mega project 2", "", "1.1.2015", "1.1.2017");
        @NotNull final List<Project> projects = projectEndpoint.findProjectsSortedByFinishDate(token);
        @NotNull XMLGregorianCalendar prevTime = projects.get(0).getFinishDate();
        for (int i = 1; i < projects.size(); i++) {
            @NotNull XMLGregorianCalendar currTime = projects.get(i).getFinishDate();
            assertFalse(prevTime.compare(currTime) > 0);
            prevTime = currTime;
        }
    }

    @Test
    void removeProject() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        assertNotNull(taskEndpoint);
        @NotNull final Project project = projectEndpoint.findProjectByName(token, "demo project");
        taskEndpoint.removeProjectTasks(token, project.getId());
        projectEndpoint.removeProject(token, project.getId());
        @NotNull final List<Project> projects = projectEndpoint.findAllProjectsByUserId(token);
        for (@NotNull Project project1 : projects) {
            assertNotEquals("demo project", project1.getName());
        }
    }

    @Test
    void persistProject() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        projectEndpoint.persistProject(token, "demo project 2", "desc", "1.1.2019", "1.1.2020");
        @NotNull final List<Project> projects = projectEndpoint.findAllProjectsByUserId(token);
        for (@NotNull final Project project : projects) {
            if (project.getName().equals("demo project 2")) {
                assertEquals("demo project 2", project.getName());
            }
        }
        assertFalse(projects.isEmpty());
    }

    @Test
    void findProjectByName() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        projectEndpoint.persistProject(token, "mega project 3", "", "3.3.2020", "12.12.2025");
        assertNotNull(projectEndpoint);
        @NotNull final Project project = projectEndpoint.findProjectByName(token, "mega project 3");
        assertNotNull(project);
    }

    @Test
    void findProjectById() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        @NotNull final Project project = projectEndpoint.findProjectByName(token, "demo project");
        assertNotNull(projectEndpoint.findProjectById(token, project.getId()));
    }

    @Test
    void findProjectsSortedByStartDate() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        projectEndpoint.persistProject(token, "mega project 4", "", "1.1.2012", "1.1.2017");
        @NotNull final List<Project> projects = projectEndpoint.findProjectsSortedByStartDate(token);
        @NotNull XMLGregorianCalendar prevTime = projects.get(0).getStartDate();
        for (int i = 1; i < projects.size(); i++) {
            @NotNull XMLGregorianCalendar currTime = projects.get(i).getStartDate();
            assertFalse(prevTime.compare(currTime) > 0);
            prevTime = currTime;
        }
    }

    @Test
    void findProjectsBySearch() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        projectEndpoint.persistProject(token, "mini project 2", "", "1.1.2009", "1.1.2010");
        projectEndpoint.persistProject(token, "super project 3", "desc mini", "1.1.2013", "1.1.2016");
        @NotNull final List<Project> projects = projectEndpoint.findProjectsBySearch(token, "mini");
        for (@NotNull final Project project : projects) {
            assertTrue(project.getName().contains("mini") || project.getDescription().contains("mini"));
        }
    }

    @Test
    void mergeProject() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        @NotNull final Project project = projectEndpoint.findProjectByName(token, "demo project");
        projectEndpoint.mergeProject(token, project.getId(), "super project",
                project.getDescription(), "1.1.2014", "1.1.2023");
        @NotNull final Project tempProject = projectEndpoint.findProjectByName(token, "super project");
        assertNotEquals(project.getName(), tempProject.getName());
    }

    @Test
    void findProjectsSortedByCreateDate() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        projectEndpoint.persistProject(token, "mega project 2", "", "1.1.2018", "1.1.2022");
        @NotNull final List<Project> projects = projectEndpoint.findProjectsSortedByCreateDate(token);
        @NotNull XMLGregorianCalendar prevTime = projects.get(0).getCreateDate();
        for (int i = 1; i < projects.size(); i++) {
            @NotNull XMLGregorianCalendar currTime = projects.get(i).getCreateDate();
            assertFalse(prevTime.compare(currTime) > 0);
            prevTime = currTime;
        }
    }

    @Test
    void removeAllProjects() throws Exception_Exception {
        assertNotNull(projectEndpoint);
        projectEndpoint.persistProject(token, "mega project 2", "", "1.1.2018", "1.1.2022");
        projectEndpoint.removeAllProjects(token);
        @NotNull final List<Project> projects = projectEndpoint.findAllProjectsByUserId(token);
        assertTrue(projects.isEmpty());
    }
}