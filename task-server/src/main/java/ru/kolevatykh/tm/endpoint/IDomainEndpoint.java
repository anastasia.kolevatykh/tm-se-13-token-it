package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    @NotNull String URL = "http://0.0.0.0:1234/DomainEndpoint?wsdl";

    @WebMethod
    void serialize(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception;

    @WebMethod
    void deserialize(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception;

    @WebMethod
    void saveJacksonJson(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception;

    @WebMethod
    void loadJacksonJson(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception;

    @WebMethod
    void saveJacksonXml(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception;

    @WebMethod
    void loadJacksonXml(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception;

    @WebMethod
    void marshalJaxbJson(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception;

    @WebMethod
    void unmarshalJaxbJson(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception;

    @WebMethod
    void marshalJaxbXml(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception;

    @WebMethod
    void unmarshalJaxbXml(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception;
}
