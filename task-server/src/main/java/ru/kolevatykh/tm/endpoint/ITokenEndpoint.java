package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITokenEndpoint {

    @NotNull String URL = "http://0.0.0.0:1234/TokenEndpoint?wsdl";

    @WebMethod
    void closeTokenSession(
            @WebParam(name = "token") @Nullable String tokenString
    ) throws Exception;

    @WebMethod
    void closeAllTokenSession(
            @WebParam(name = "token") @Nullable String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Session> getListTokenSession(
            @WebParam(name = "token") @Nullable String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    User getUserByToken(
            @WebParam(name = "token") @Nullable String tokenString
    ) throws Exception;

    @Nullable
    @WebMethod
    String openTokenSessionAuth(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    ) throws Exception;

    @Nullable
    @WebMethod
    String openTokenSessionReg(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    ) throws Exception;
}
