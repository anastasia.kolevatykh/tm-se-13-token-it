package ru.kolevatykh.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor(force = true)
public abstract class AbstractEntity implements Serializable {

//    @XmlElement
    @NotNull
    protected String id = UUID.randomUUID().toString();
}
