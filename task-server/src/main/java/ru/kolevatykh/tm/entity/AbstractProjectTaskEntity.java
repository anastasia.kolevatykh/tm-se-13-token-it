package ru.kolevatykh.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.enumerate.StatusType;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractProjectTaskEntity extends AbstractEntity implements Serializable {

    @NotNull
    protected String userId;

    @NotNull
    protected String name = "Placeholder";

    @NotNull
    protected String description = "Placeholder";

    @NotNull
    protected Date createDate = new Date(System.currentTimeMillis());

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Nullable
    protected Date startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Nullable
    protected Date finishDate;

    @NotNull
    protected StatusType statusType = StatusType.PLANNED;

    AbstractProjectTaskEntity(@NotNull final String name, @NotNull final String description,
                              @Nullable final Date startDate, @Nullable final Date finishDate) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.finishDate = finishDate;
    }
}
