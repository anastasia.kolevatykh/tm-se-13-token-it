package ru.kolevatykh.tm.api;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Project;
import java.util.List;

public interface IProjectRepository extends IProjectTaskRepository<Project> {

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_project;")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Project> findAll() throws Exception;

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = #{userId};")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Project> findAllByUserId(@NotNull String userId) throws Exception;

    @Nullable
    @Select("SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = #{userId} AND id = #{id};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    Project findOneById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id) throws Exception;

    @Nullable
    @Select("SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = #{userId} AND name = #{name};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    Project findOneByName(@Param("userId") @NotNull String userId, @Param("name") @NotNull String name) throws Exception;

    @Insert("INSERT INTO `my-task-manager_db`.app_project (`id`, `user_id`, `name`, `description`, "
            + "`create_date`, `start_date`, `finish_date`, `status_type`) VALUES (#{id}, #{userId}, "
            + "#{name}, #{description}, #{createDate}, #{startDate}, #{finishDate}, #{statusType});")
    void persist(@NotNull final Project project) throws Exception;

    @Update("UPDATE `my-task-manager_db`.app_project SET name = #{name}, description = #{description}," +
            "create_date = #{createDate}, start_date = #{startDate}, finish_date = #{finishDate}, "
            + "status_type = #{statusType} WHERE id = #{id};")
    void merge(@NotNull final Project project) throws Exception;

    @Delete("DELETE FROM `my-task-manager_db`.app_project WHERE user_id = #{userId} AND id = #{id};")
    void remove(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id) throws Exception;

    @Delete("DELETE FROM `my-task-manager_db`.app_project WHERE user_id = #{userId};")
    void removeAllByUserId(@NotNull String userId) throws Exception;

    @Delete("DELETE FROM `my-task-manager_db`.app_project;")
    void removeAll() throws Exception;

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = #{userId} ORDER BY create_date ASC;")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Project> findAllSortedByCreateDate(@NotNull final String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = #{userId} ORDER BY start_date ASC;")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Project> findAllSortedByStartDate(@NotNull final String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = #{userId} ORDER BY finish_date ASC;")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Project> findAllSortedByFinishDate(@NotNull final String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = #{userId} "
            + "ORDER BY FIELD(status_type, 'PLANNED', 'INPROCESS', 'READY');")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Project> findAllSortedByStatus(@NotNull final String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_project WHERE user_id = #{userId} " +
            "AND (name LIKE #{search} OR description LIKE #{search});")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Project> findAllBySearch(@Param("userId") @NotNull final String userId,
                                  @Param("search") @NotNull final String search) throws Exception;
}
