package ru.kolevatykh.tm.api;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Task;
import java.util.List;

public interface ITaskRepository extends IProjectTaskRepository<Task> {

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_task;")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Task> findAll() throws Exception;

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = #{userId};")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Task> findAllByUserId(@NotNull String userId) throws Exception;

    @Nullable
    @Select("SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = #{userId} AND id = #{id};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    Task findOneById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id) throws Exception;

    @Nullable
    @Select("SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = #{userId} AND name = #{name};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    Task findOneByName(@Param("userId") @NotNull String userId, @Param("name") @NotNull String name) throws Exception;

    @Insert("INSERT INTO `my-task-manager_db`.app_task (`id`, `user_id`, `project_id`, `name`, `description`, "
            + "`create_date`, `start_date`, `finish_date`, `status_type`) VALUES (#{id}, #{userId}, #{projectId}, "
            + "#{name}, #{description}, #{createDate}, #{startDate}, #{finishDate}, #{statusType});")
    void persist(@NotNull Task task) throws Exception;

    @Update("UPDATE `my-task-manager_db`.app_task SET project_id = #{projectId}, name = #{name}, "
            + "description = #{description}, create_date = #{createDate}, start_date = #{startDate}, "
            + "finish_date = #{finishDate}, status_type = #{statusType} WHERE id = #{id};")
    void merge(@NotNull Task task) throws Exception;

    @Delete("DELETE FROM `my-task-manager_db`.app_task WHERE user_id = #{userId} AND id = #{id};")
    void remove(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id) throws Exception;

    @Delete("DELETE FROM `my-task-manager_db`.app_task WHERE user_id = #{userId};")
    void removeAllByUserId(@NotNull String userId) throws Exception;

    @Delete("DELETE FROM `my-task-manager_db`.app_task;")
    void removeAll() throws Exception;

    @Delete("DELETE FROM `my-task-manager_db`.app_task WHERE user_id = #{userId} AND project_id IS NOT NULL;")
    void removeTasksWithProjectId(@NotNull String userId) throws Exception;

    @Delete("DELETE FROM `my-task-manager_db`.app_task WHERE user_id = #{userId} AND project_id = #{projectId};")
    void removeProjectTasks(@Param("userId") @NotNull String userId,
                            @Param("projectId") @NotNull String projectId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = #{userId} AND project_id = #{projectId};")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Task> findTasksByProjectId(@Param("userId") @NotNull String userId,
                                    @Param("projectId") @NotNull String projectId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = #{userId} AND project_id IS NULL;")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Task> findTasksWithoutProject(@NotNull String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = #{userId} ORDER BY create_date ASC;")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Task> findAllSortedByCreateDate(@NotNull String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = #{userId} ORDER BY start_date ASC;")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Task> findAllSortedByStartDate(@NotNull String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = #{userId} ORDER BY finish_date ASC;")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Task> findAllSortedByFinishDate(@NotNull String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = #{userId} "
            + "ORDER BY FIELD(status_type, 'PLANNED', 'INPROCESS', 'READY');")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Task> findAllSortedByStatus(@NotNull String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_task WHERE user_id = #{userId} " +
            "AND (name LIKE #{search} OR description LIKE #{search});")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "createDate", column = "create_date"),
            @Result(property = "startDate", column = "start_date"),
            @Result(property = "finishDate", column = "finish_date"),
            @Result(property = "statusType", column = "status_type")
    })
    List<Task> findAllBySearch(@Param("userId") @NotNull final String userId,
                               @Param("search") @NotNull final String search) throws Exception;
}
