package ru.kolevatykh.tm.api;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.User;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_user;")
    @Results({
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "roleType", column = "role_type"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    List<User> findAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `my-task-manager_db`.app_user WHERE id = #{id};")
    @Results({
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "roleType", column = "role_type"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    User findOneById(@NotNull final String id) throws Exception;

    @Nullable
    @Select("SELECT * FROM `my-task-manager_db`.app_user WHERE login = #{login};")
    @Results({
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "roleType", column = "role_type"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    User findOneByLogin(@NotNull final String login) throws Exception;

    @Insert("INSERT INTO `my-task-manager_db`.app_user (`id`, `login`, `password_hash`, `role_type`,"
            + "`email`, `first_name`, `last_name`, `middle_name`, `phone`, `locked`) "
            + "VALUES (#{id}, #{login}, #{passwordHash}, #{roleType}, #{email}, "
            + "#{firstName}, #{lastName}, #{middleName}, #{phone}, #{locked});")
    void persist(@NotNull final User user) throws Exception;

    @Update("UPDATE `my-task-manager_db`.app_user SET login = #{login}, "
            + "password_hash = #{passwordHash}, role_type = #{roleType}, email = #{email}, "
            + "first_name = #{firstName}, last_name = #{lastName}, middle_name = #{middleName},"
            + " phone = #{phone}, locked = #{locked} WHERE id = #{id};")
    void merge(@NotNull final User user) throws Exception;

    @Delete("DELETE FROM `my-task-manager_db`.app_user WHERE id = #{id};")
    void remove(@NotNull final String id) throws Exception;

    @Delete("DELETE FROM `my-task-manager_db`.app_user;")
    void removeAll() throws Exception;
}
