package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.Nullable;
import java.util.List;

public interface IService<T> {

    @Nullable List<T> findAll() throws Exception;

    @Nullable T findOneById(@Nullable String id) throws Exception;

    void remove(@Nullable String id) throws Exception;

    void removeAll() throws Exception;
}
