package ru.kolevatykh.tm.api;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Session;
import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull
    @Select("SELECT * FROM `my-task-manager_db`.app_session;")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "roleType", column = "role_type"),
    })
    List<Session> findAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `my-task-manager_db`.app_session WHERE id = #{id};")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "roleType", column = "role_type"),
    })
    Session findOneById(@NotNull String id) throws Exception;

    @Insert("INSERT INTO `my-task-manager_db`.app_session (`id`, `signature`, "
            + "`timestamp`, `user_id`, `role_type`) VALUES (#{id}, #{signature}, "
            + "#{timestamp}, #{userId}, #{roleType});")
    void persist(@NotNull Session session) throws Exception;

    @Update("UPDATE `my-task-manager_db`.app_session SET signature = #{signature}, "
            + "timestamp = #{timestamp}, user_id = #{userId}, "
            + "role_type = #{roleType} WHERE id = #{id};")
    void merge(@NotNull Session session) throws Exception;

    @Delete("DELETE FROM `my-task-manager_db`.app_session WHERE id = #{id};")
    void remove(@NotNull String id) throws Exception;

    @Delete("DELETE FROM `my-task-manager_db`.app_session;")
    void removeAll() throws Exception;

    @Delete("DELETE FROM `my-task-manager_db`.app_session WHERE user_id = #{userId};")
    void removeAllByUserId(@NotNull final String userId) throws Exception;
}
