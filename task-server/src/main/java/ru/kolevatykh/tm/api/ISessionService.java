package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;

public interface ISessionService extends IService<Session> {

    @Nullable User getUser(@Nullable Session session) throws Exception;

    @NotNull Session openAuth(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull Session openReg(@NotNull String login, @NotNull String password) throws Exception;

    void validate(@Nullable Session session) throws Exception;

    void removeAllByUserId(@Nullable final String userId) throws Exception;
}
