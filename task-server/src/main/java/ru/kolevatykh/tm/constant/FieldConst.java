package ru.kolevatykh.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class FieldConst {

    @NotNull public static final String ID = "id";

    @NotNull public static final String SIGNATURE = "signature";
    @NotNull public static final String TIMESTAMP = "timestamp";
    @NotNull public static final String ROLE_TYPE = "role_type";

    @NotNull public static final String LOGIN = "login";
    @NotNull public static final String PASSWORD_HASH = "password_hash";
    @NotNull public static final String LOCKED = "locked";
    @NotNull public static final String EMAIL = "email";
    @NotNull public static final String PHONE = "phone";
    @NotNull public static final String FIRST_NAME = "first_name";
    @NotNull public static final String LAST_NAME = "last_name";
    @NotNull public static final String MIDDLE_NAME = "middle_name";

    @NotNull public static final String USER_ID = "user_id";
    @NotNull public static final String PROJECT_ID = "project_id";
    @NotNull public static final String NAME = "name";
    @NotNull public static final String DESCRIPTION = "description";
    @NotNull public static final String CREATE_DATE = "create_date";
    @NotNull public static final String START_DATE = "start_date";
    @NotNull public static final String FINISH_DATE = "finish_date";
    @NotNull public static final String STATUS_TYPE = "status_type";
}

