package ru.kolevatykh.tm.enumerate;

import org.jetbrains.annotations.NotNull;

public enum StatusType {
    PLANNED("PLANNED"),
    INPROCESS("INPROCESS"),
    READY("READY");

    @NotNull
    private final String displayName;

    StatusType(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    public String displayName(){
        return displayName;
    }
}
