package ru.kolevatykh.tm.enumerate;

import org.jetbrains.annotations.NotNull;

public enum RoleType {
    ADMIN("ADMIN"),
    USER("USER");

    @NotNull private final String displayName;

    RoleType(@NotNull final String roleName) {
        this.displayName = roleName;
    }

    public String displayName(){
        return displayName;
    }
}
