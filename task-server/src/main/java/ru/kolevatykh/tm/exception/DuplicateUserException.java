package ru.kolevatykh.tm.exception;

import org.jetbrains.annotations.NotNull;

public class DuplicateUserException extends Exception {

    @NotNull
    private static final String ERROR_MESSAGE = "[The login already exists.]";

    public DuplicateUserException() {
        super(ERROR_MESSAGE);
    }
}
