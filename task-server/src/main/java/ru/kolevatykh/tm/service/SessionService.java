package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ISessionService;
import ru.kolevatykh.tm.api.IUserService;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.api.ISessionRepository;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.exception.*;
import ru.kolevatykh.tm.util.ConfigUtil;
import ru.kolevatykh.tm.util.MyBatisUtil;
import ru.kolevatykh.tm.util.SignatureUtil;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private IUserService userService;

    public SessionService(@NotNull final IUserService userService) {
        this.setUserService(userService);
    }

    @Override
    @Nullable
    public User getUser(@Nullable final Session session) throws Exception {
        if (session == null) return null;
        @Nullable final User user = userService.findOneById(session.getUserId());
        return user;
    }

    @Override
    @NotNull
    public Session openAuth(@NotNull final String login, @NotNull final String password) throws Exception {
        @Nullable User user = userService.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (!user.getPasswordHash().equals(password)) throw new InvalidPasswordException();
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRoleType(user.getRoleType());
        session.setSignature(SignatureUtil.sign(session, ConfigUtil.getSalt(), 13));
        persist(session);
        return session;
    }

    @Override
    @NotNull
    public Session openReg(@NotNull final String login, @NotNull final String password) throws Exception {
        if (userService.findOneByLogin(login) != null) throw new DuplicateUserException();
        @Nullable User user = new User(login, password, RoleType.USER);
        userService.persist(user);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRoleType(user.getRoleType());
        session.setSignature(SignatureUtil.sign(session, ConfigUtil.getSalt(), 13));
        persist(session);
        return session;
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) return;
        @NotNull final long timeOut = 5 * 60 * 1000L;
        if (System.currentTimeMillis() - session.getTimestamp() > timeOut) {
            remove(session.getId());
            throw new SessionExpiredException();
        }
        @NotNull final String signature = Session.generateSignature(session);
        if (!signature.equals(session.getSignature()))
            throw new SignatureCorruptException("Session");
        if (findOneById(session.getId()) == null)
            throw new SessionNotFoundException();
    }

    @Override
    public @Nullable List<Session> findAll() throws Exception {
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sql.getMapper(ISessionRepository.class);
        @NotNull final List<Session> list = sessionRepository.findAll();
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sql.getMapper(ISessionRepository.class);
        @Nullable final Session session = sessionRepository.findOneById(id);
        if (session == null) return null;
        return session;
    }

    @Override
    void persist(@Nullable final Session session) throws Exception {
        if (session == null) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sql.getMapper(ISessionRepository.class);
        try {
            sessionRepository.persist(session);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    void merge(@Nullable final Session session) throws Exception {
        if (session == null) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sql.getMapper(ISessionRepository.class);
        try {
            sessionRepository.merge(session);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sql.getMapper(ISessionRepository.class);
        try {
            sessionRepository.remove(id);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sql.getMapper(ISessionRepository.class);
        try {
            sessionRepository.removeAll();
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository sessionRepository = sql.getMapper(ISessionRepository.class);
        try {
            sessionRepository.removeAllByUserId(userId);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }
}
