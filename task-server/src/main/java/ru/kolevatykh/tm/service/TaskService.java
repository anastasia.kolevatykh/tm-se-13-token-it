package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ITaskRepository;
import ru.kolevatykh.tm.api.ITaskService;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.util.MyBatisUtil;
import java.util.List;

@Getter
@Setter
public final class TaskService extends AbstractProjectTaskService<Task> implements ITaskService {

    @Nullable
    @Override
    public final List<Task> findAll() throws Exception {
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        @NotNull final List<Task> list = taskRepository.findAll();
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        @NotNull final List<Task> list = taskRepository.findAllByUserId(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final Task findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        return taskRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public final Task findOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty() || userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    public void persist(@Nullable final Task task) throws Exception {
        if (task == null) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        try {
            taskRepository.persist(task);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void merge(@Nullable final Task task) throws Exception {
        if (task == null) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        try {
            taskRepository.merge(task);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        try {
            taskRepository.remove(userId, id);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeAllByUserId(userId);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeAll();
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Nullable
    @Override
    public final List<Task> findAllSortedByCreateDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        @NotNull final List<Task> list = taskRepository.findAllSortedByCreateDate(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findAllSortedByStartDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        @NotNull final List<Task> list = taskRepository.findAllSortedByStartDate(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findAllSortedByFinishDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        @NotNull final List<Task> list = taskRepository.findAllSortedByFinishDate(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findAllSortedByStatus(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        @NotNull final List<Task> list = taskRepository.findAllSortedByStatus(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findAllBySearch(@Nullable final String userId, @Nullable String search) throws Exception {
        if (userId == null || userId.isEmpty() || search == null || search.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        search = '%' + search + '%';
        @NotNull final List<Task> list = taskRepository.findAllBySearch(userId, search);
        if (list.isEmpty()) return null;
        return list;
    }

    @Override
    public void removeTasksWithProjectId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeTasksWithProjectId(userId);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void removeProjectTasks(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty() || userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeProjectTasks(userId, projectId);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Nullable
    @Override
    public final List<Task> findTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty() || projectId == null || projectId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        @NotNull final List<Task> list = taskRepository.findTasksByProjectId(userId, projectId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Task> findTasksWithoutProject(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository taskRepository = sql.getMapper(ITaskRepository.class);
        @NotNull final List<Task> list = taskRepository.findTasksWithoutProject(userId);
        if (list.isEmpty()) return null;
        return list;
    }
}
