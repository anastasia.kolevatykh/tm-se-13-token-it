package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IProjectRepository;
import ru.kolevatykh.tm.api.IProjectService;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.util.MyBatisUtil;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public final class ProjectService extends AbstractProjectTaskService<Project> implements IProjectService {

    @Nullable
    @Override
    public final List<Project> findAll() throws Exception {
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository projectRepository = sql.getMapper(IProjectRepository.class);
        @NotNull final List<Project> list = projectRepository.findAll();
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Project> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository projectRepository = sql.getMapper(IProjectRepository.class);
        @NotNull final List<Project> list = projectRepository.findAllByUserId(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final Project findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository projectRepository = sql.getMapper(IProjectRepository.class);
        return projectRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public final Project findOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty() || userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository projectRepository = sql.getMapper(IProjectRepository.class);
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    public void persist(@Nullable final Project project) throws Exception {
        if (project == null) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository projectRepository = sql.getMapper(IProjectRepository.class);
        try {
            projectRepository.persist(project);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void merge(@Nullable final Project project) throws Exception {
        if (project == null) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository projectRepository = sql.getMapper(IProjectRepository.class);
        try {
            projectRepository.merge(project);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository projectRepository = sql.getMapper(IProjectRepository.class);
        try {
            projectRepository.remove(userId, id);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository projectRepository = sql.getMapper(IProjectRepository.class);
        try {
            projectRepository.removeAllByUserId(userId);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository projectRepository = sql.getMapper(IProjectRepository.class);
        try {
            projectRepository.removeAll();
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Nullable
    @Override
    public final List<Project> findAllSortedByCreateDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository projectRepository = sql.getMapper(IProjectRepository.class);
        @NotNull final List<Project> list = projectRepository.findAllSortedByCreateDate(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Project> findAllSortedByStartDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository projectRepository = sql.getMapper(IProjectRepository.class);
        @NotNull final List<Project> list = projectRepository.findAllSortedByStartDate(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Project> findAllSortedByFinishDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository projectRepository = sql.getMapper(IProjectRepository.class);
        @NotNull final List<Project> list = projectRepository.findAllSortedByFinishDate(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Project> findAllSortedByStatus(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository projectRepository = sql.getMapper(IProjectRepository.class);
        @NotNull final List<Project> list = projectRepository.findAllSortedByStatus(userId);
        if (list.isEmpty()) return null;
        return list;
    }

    @Nullable
    @Override
    public final List<Project> findAllBySearch(@Nullable final String userId, @Nullable String search) throws Exception {
        if (userId == null || userId.isEmpty() || search == null || search.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository projectRepository = sql.getMapper(IProjectRepository.class);
        search = '%' + search + '%';
        @NotNull final List<Project> list = projectRepository.findAllBySearch(userId, search);
        if (list.isEmpty()) return null;
        return list;
    }
}
