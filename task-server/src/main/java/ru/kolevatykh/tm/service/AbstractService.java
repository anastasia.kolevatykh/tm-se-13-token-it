package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.AbstractEntity;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> {

    @Nullable
    abstract List<T> findAll() throws Exception;

    @Nullable
    abstract T findOneById(@Nullable final String id) throws Exception;

    abstract void persist(@NotNull final T entity) throws Exception;

    abstract void merge(@NotNull final T entity) throws Exception;

    abstract void remove(@Nullable final String id) throws Exception;

    abstract void removeAll() throws Exception;
}
