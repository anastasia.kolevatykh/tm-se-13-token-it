package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IUserRepository;
import ru.kolevatykh.tm.api.IUserService;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.util.MyBatisUtil;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @Override
    public @Nullable List<User> findAll() throws Exception {
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IUserRepository userRepository = sql.getMapper(IUserRepository.class);
        @NotNull final List<User> list = userRepository.findAll();
        if (list.isEmpty()) return null;
        return list;
    }

    @Override
    public @Nullable User findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IUserRepository userRepository = sql.getMapper(IUserRepository.class);
        @Nullable final User user = userRepository.findOneById(id);
        if (user == null) return null;
        return user;
    }

    @Nullable
    @Override
    public final User findOneByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return null;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IUserRepository userRepository = sql.getMapper(IUserRepository.class);
        @Nullable final User user = userRepository.findOneByLogin(login);
        if (user == null) return null;
        return user;
    }

    @Override
    public void persist(@Nullable final User user) throws Exception {
        if (user == null) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IUserRepository userRepository = sql.getMapper(IUserRepository.class);
        try {
            userRepository.persist(user);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void merge(@Nullable User user) throws Exception {
        if (user == null) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IUserRepository userRepository = sql.getMapper(IUserRepository.class);
        try {
            userRepository.merge(user);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void remove(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IUserRepository userRepository = sql.getMapper(IUserRepository.class);
        try {
            userRepository.remove(id);
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sql = MyBatisUtil.getSqlSessionFactory().openSession();
        @NotNull final IUserRepository userRepository = sql.getMapper(IUserRepository.class);
        try {
            userRepository.removeAll();
            sql.commit();
        } catch (Exception e) {
            sql.rollback();
        } finally {
            sql.close();
        }
    }
}
