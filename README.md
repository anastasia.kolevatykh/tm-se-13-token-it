# Task Manager

### Описание

Приложение для создания списков задач.

### Требования к software:
1. Java 1.8

### Технологический стек:
1. Java 1.8
2. Maven 3.8.0

### Имя и контакты разработчика

Anastasia Kolevatykh

anastasia.kolevatykh@gmail.com

### Команды для сборки приложения:
```
mvn clean install
```
### Команды для запуска приложения:
```
java -jar target/task-manager-1.0.0.jar
```

### Команды для создания таблиц в базе данных:

```
    CREATE TABLE `app_user` (
        `id` VARCHAR(255) NOT NULL,
        `login` VARCHAR(255) NULL DEFAULT NULL,
        `password_hash` VARCHAR(255) NULL DEFAULT NULL,
        `role_type` VARCHAR(255) NULL DEFAULT NULL,
        `email` VARCHAR(255) NULL DEFAULT NULL,
        `first_name` VARCHAR(255) NULL DEFAULT NULL,
        `last_name` VARCHAR(255) NULL DEFAULT NULL,
        `middle_name` VARCHAR(255) NULL DEFAULT NULL,
        `phone` VARCHAR(255) NULL DEFAULT NULL,
        `locked` BIT(1) NULL DEFAULT NULL,
        PRIMARY KEY (`id`)
    ) COLLATE='utf8_general_ci' ENGINE=InnoDB;

    CREATE TABLE `app_session` (
        `id` VARCHAR(255) NOT NULL,
        `signature` VARCHAR(255) NULL DEFAULT NULL,
        `timestamp` BIGINT(20) NULL DEFAULT NULL,
        `user_id` VARCHAR(255) NULL DEFAULT NULL,
        `role_type` VARCHAR(255) NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        INDEX `FKrrdhu5ryvprfqplat774p2n4t` (`user_id`),
        CONSTRAINT `FKrrdhu5ryvprfqplat774p2n4t` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`)
    ) COLLATE='utf8_general_ci' ENGINE=InnoDB;

    CREATE TABLE `app_project` (
        `id` VARCHAR(255) NOT NULL,
        `user_id` VARCHAR(255) NULL DEFAULT NULL,
        `name` VARCHAR(255) NULL DEFAULT NULL,
        `description` VARCHAR(255) NULL DEFAULT NULL,
        `create_date` DATETIME NULL DEFAULT NULL,
        `start_date` DATETIME NULL DEFAULT NULL,
        `finish_date` DATETIME NULL DEFAULT NULL,
        `status_type` VARCHAR(255) NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        INDEX `FKp9byv3k2r7rgg7svn3rx10a1u` (`user_id`),
        CONSTRAINT `FKp9byv3k2r7rgg7svn3rx10a1u` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`)
    ) COLLATE='utf8_general_ci' ENGINE=InnoDB;

    CREATE TABLE `app_task` (
        `id` VARCHAR(255) NOT NULL,
        `user_id` VARCHAR(255) NULL DEFAULT NULL,
        `project_id` VARCHAR(255) NULL DEFAULT NULL,
        `name` VARCHAR(255) NULL DEFAULT NULL,
        `description` VARCHAR(255) NULL DEFAULT NULL,
        `create_date` DATETIME NULL DEFAULT NULL,
        `start_date` DATETIME NULL DEFAULT NULL,
        `finish_date` DATETIME NULL DEFAULT NULL,
        `status_type` VARCHAR(255) NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        INDEX `FKsu3pcsyuwrs6nmpcpufikq5u4` (`project_id`),
        INDEX `FKkc5pwubxw7j4b0xprgdmgkrel` (`user_id`),
        CONSTRAINT `FKkc5pwubxw7j4b0xprgdmgkrel` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`),
        CONSTRAINT `FKsu3pcsyuwrs6nmpcpufikq5u4` FOREIGN KEY (`project_id`) REFERENCES `app_project` (`id`)
    ) COLLATE='utf8_general_ci' ENGINE=InnoDB;
```

